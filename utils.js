var bodyParser = require('body-parser');
//var csrf = require('csurf');
var yaml = require("js-yaml");
var fs = require("fs");
var e = yaml.load(fs.readFileSync("config.yml"));
var express = require('express');
var mongoose = require('mongoose');
var session = require('client-sessions');

var middleware = require('./middleware');
var authorize = require('./service/authorize');

module.exports.createUserSession = function(req, res, user) {
  var cleanUser = {
    firstName:  user.firstName,
    lastName:   user.lastName,
    email:      user.email,
    data:       user.data || {},
  };

  req.session.user = cleanUser;
  req.user = cleanUser;
  res.locals.user = cleanUser;
};

module.exports.createAdminSession = function(req, res, user) {
  var cleanUser = {
    firstName:  user.firstName,
    lastName:   user.lastName,
    email:      user.email,
    data:       user.data || {},
  };

  req.session.admin = cleanUser;
  req.admin = cleanUser;
  res.locals.admin = cleanUser;
};

module.exports.createApp = function() {

  var options = {
  db: { native_parser: true },
  server: { poolSize: 1 }
}
  mongoose.connect(e.mongoUrl,options);

  var app = express();
var http = require('http').Server(app);
 exports.io = require('socket.io')(http);
  // settings
  app.set('view engine', 'ejs');

  // middleware
  app.use(bodyParser.urlencoded({ extended: true }));
  app.use(bodyParser.json());
  app.use(session({
    cookieName: 'session',
    secret: 'keyboard cat',
    duration: 30 * 60 * 1000,
    activeDuration: 5 * 60 * 1000,
  }));
  //app.use(csrf());
  app.use(middleware.simpleAuth);

  // routes
  app.use(require('./routes/auth'));
  app.use(require('./routes/main'));
  app.use(express.static(process.cwd() + '/public'));
 app.use('/api/graph',require('./routes/api'));



authorize.neo4jAuthorize(e.neo4jUser, e.neo4jPassword);
authorize.setloginAuthentication(e.loginUser, e.loginPassword);
  
  return app;
};

module.exports.requireLogin = function(req, res, next) {
  if (!req.user) {
    res.redirect('/login');
  } else {
    next();
  }
};
