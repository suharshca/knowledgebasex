var getJSON = require('../getJSON');
var authorize = require('./authorize');
var stringHelper = require('../helpers/stringHelper');

exports.insertNode = function (nodeType ,properties){
	var statement = "CREATE (n:" + nodeType + properties ")"
	var post_data = '{"statements":[{"statement":"%s","resultDataContents":[""]}]}';
    post_data = stringHelper.parseString(post_data,statement);
    var options = {
        host: 'localhost',
        port: 7474,
        path: '/db/data/transaction/commit',
        method: 'POST',
        headers: {
            'Content-Type': 'application/json',
            'Content-Length': post_data.length,
            'Authorization': authorize.getNeo4jAuthorizationHeader()
        }
    };
    getJSON.getJSON(options, post_data,
        function(statusCode, result) {
            if (statusCode == 200) {} else {}
            var temp = converts.convertGraph(result);
            res.send(temp);
        });
}