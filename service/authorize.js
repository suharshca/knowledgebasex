var getJSON = require('../getJSON');

var neo4jAuthorizationHeader;
var loginAuthenticationHeader;


exports.neo4jAuthorize = function(username, password) {
    neo4jAuthorizationHeader = new Buffer(username + ':' + password).toString('base64');
    var options = {
        host: 'localhost',
        port: 7474,
        path: '/user/neo4j',
        method: 'GET',
        headers: {
            'Content-Type': 'application/json',
            'Authorization': neo4jAuthorizationHeader
        }
    };

    getJSON.getJSON(options, "",
        function(statusCode, result) {
            if (statusCode == 200) {
                console.log("Authorized with Neo4j \' " + JSON.stringify(result) + "\'");
            } else {
                console.log("Error!! couldnot Authorize");
            }
            return;
        });
}

exports.dbAuthorize = function(username, password, database) {


}

exports.setloginAuthentication = function(username, password) {
    loginAuthenticationHeader = {user :username , pass: password };
}

exports.loginAuthenticate = function (username , password) {
    if(!loginAuthenticationHeader.user.localeCompare(username)){
        if(!loginAuthenticationHeader.pass.localeCompare(password)){
            return true;
        }
    }
    return false;
}

exports.getNeo4jAuthorizationHeader = function() {
    return neo4jAuthorizationHeader;
}
