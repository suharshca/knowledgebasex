exports.convertGraph = function(res) {
    function hasNodeAdded(a, id) {
        for (var i = 0; i < a.length; i++) {
            if (a[i].id == id) return i;
        }
        return null;
    }

    function hasRelationAdded(a, id) {
        for (var i = 0; i < a.length; i++) {
            if (a[i].id == id) return i;
        }
        return null;
    }

    var nodes = [],
        links = [];
    res.results[0].data.forEach(function(row) {
        row.graph.nodes.forEach(function(n) {
            if (hasNodeAdded(nodes, n.id) == null)
                nodes.push({
                    id: n.id,
                    labels: n.labels,
                    properties: n.properties
                });
        });

        row.graph.relationships.forEach(function(r) {
            if (hasRelationAdded(links, r.id) == null)
                links.push({
                    id: r.id,
                    source: r.startNode,
                    target: r.endNode,
                    type: r.type,
                    properties: r.properties
                });
        });
    });
    viz = {
        nodes: nodes,
        links: links
    };
    return viz;
};

exports.convertTable = function(res) {
    var rows = [];
    res.results[0].data.forEach(function(row) {
        rows.push(row.row);
    });
    var viz = {
        columns: res.results[0].columns,
        rows: rows
    }
    return viz;
};
