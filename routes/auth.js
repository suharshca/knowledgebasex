var bodyParser = require('body-parser');
var neo4j = require('node-neo4j');
var yaml = require("js-yaml");
var fs = require("fs");
var e = yaml.load(fs.readFileSync("./config.yml"));

db = new neo4j('http://' + e.neo4jUser + ':' + e.neo4jPassword + '@localhost:7474');
var bcrypt = require('bcryptjs');
var express = require('express');

var models = require('../models');
var utils = require('../utils');

var router = express.Router();
var async = require('async');
router.use(bodyParser.json());
router.use(bodyParser.urlencoded({ extended: true}));
var result;
/**
 * Render the registration page.
 */

//start

/*router.post('/hi',function(req,res,next){
  res.send('hi');
});*/


/**
 * Create a new user account.
 *
 * Once a user is logged in, they will be sent to the dashboard page.
 */

router.post('/register', function(req, res) {
    var salt = bcrypt.genSaltSync(10);
    var hash = bcrypt.hashSync(req.body.password, salt);

    var user = new models.User({
        firstName: req.body.firstName,
        lastName: req.body.lastName,
        email: req.body
            .email,
        password: hash,
    });
    user.save(function(err) {
        if (err) {
            var error = 'Something bad happened! Please try again.';

            if (err.code === 11000) {
                error = 'That email is already taken!';
            }

            res.render('register', { er: error });
        } else {
            utils.createUserSession(req, res, user);
            res.redirect('/contribute');
        }
    });
});
router.get('/register', function(req, res) {
    if (req.session.user)
        res.redirect('/contribute');
    else
        res.render('register',{er:""});
});

/**
 * Render the login page.
 */
router.get('/login', function(req, res) {
    if (req.session.user)
        res.redirect('/contribute');
    else
        res.render('login',{er:""});
});

router.get('/visual',function(req,res){
    res.render('visual2');
});

router.get('/admin',function(req,res){
    if(req.session.admin)
        res.redirect('/verify');
    else
        res.render('admin',{ er: "" });
});

router.get('/response',function(req,res){

    res.render('response',{res :result});
})



router.post('/admin', function(req, res) {
    if(req.body.email==e.admin)
    {
        models.User.findOne({ email: req.body.email }, 'firstName lastName email password data', function(err, admin) {
        if (!admin) {
            res.render('admin',{ er: "Incorrect email or password." });
        } else {
            if (bcrypt.compareSync(req.body.password, admin.password)) {
                utils.createUserSession(req, res, admin);
                res.redirect('/verify');
            } else {
                res.render('admin', { er: "Incorrect email password." });
            }
        }
        });
    }
});




/**
 * Log a user into their account.
 *
 * Once a user is logged in, they will be sent to the dashboard page.
 */
router.post('/login', function(req, res) {
    models.User.findOne({ email: req.body.email }, 'firstName lastName email password data', function(err, user) {
        if (!user) {
            res.render('login',{ er: "Incorrect email or password." });
        } else {
            if (bcrypt.compareSync(req.body.password, user.password)) {
                utils.createUserSession(req, res, user);
                res.redirect('/contribute');
            } else {
                res.render('login', { er: "Incorrect email or password." });
            }
        }
    });
});



router.post('/insert', function(req, res) {
    //console.log(req.body);
    if (req.body.name != "") {

        models.Node.findOne({ node_name: req.body.name }, function(err, n_ode) {
            if (err) throw err;
            if (n_ode) {
                console.log("already exist" + n_ode);
                res.json('');
            } else {
                db.cypherQuery("CREATE (n:" + req.body.label + "{name:'" + req.body.name + "'}) return n", function(err, result) {
                    if (err) throw err;

                    console.log(result.data[0]._id); // delivers an array of query results
                    console.log(result.columns); // delivers an array of names of objects getting returned

                    var nodes = new models.Node({
                        node_id: result.data[0]._id,
                        node_name: req.body.name,
                        label: req.body.label
                    });

                    nodes.save(function(err) {
                        if (err)
                            console.log(err);
                    });
                    res.json(result.data[0]);
                });
            }

        });
    } else {
        er = '{"err":"Fields cant be empty"}';
        var e = JSON.parse(er);
        res.json(e);

    }

});

router.post('/search', function(req, res) {

    //console.log(req.body);
    if (req.body.name != "") {
        var response = '{"resp":"';
        var q = 0,
            rl = 0;
        var add = function(resp, rl) {

            response = response + resp;
            //if(q<rl)
            response = response + ',';
            //console.log(response);
            q++;
        }
        var send = function() {
            response += '"}';
            res.json(JSON.parse(response));
            console.log(JSON.parse(response).resp);
        }
        models.Node.findOne({ node_name: req.body.name }, function(err, n_ode) {
            if (err) throw err;
            if (!n_ode) {
                console.log("search failure");
                res.json('');
            } else {

                db.readRelationshipsOfNode(n_ode.node_id, {
                    types: ['Uses', 'Harms'] // optional
                        // optional, alternative 'out', defaults to 'all'
                }, function(err, relationships) {
                    if (err) throw err;
                    //res.json(relationships);
                    console.log(relationships); // delivers an array of relationship objects.
                    if(relationships.length==0)
                    {
                        console.log("enter");
                        response = '{"resp":"Node found but No relationships exist';
                        send();

                    }
                    console.log("printing" + relationships.length);

                    async.forEachOf(relationships, function(rel, key, callback) {
                        // Perform operation on file here.
                        console.log('Processing file ' + rel);
                        models.Node.findOne({ node_id: rel._start }, function(err, node1) {
                            if (err) throw err;
                            console.log(node1);
                            if (node1) {

                                console.log(JSON.stringify(rel) + "bla");
                                models.Node.findOne({ node_id: rel._end }, function(err, node2) {
                                    if (err) throw err;
                                    props = rel.prop.split(',');

                                    for (i = 0; i < props.length; i++) {
                                        if (rel._type == 'Harms')
                                            var resp = node1.node_name + " " + rel._type + " " + node2.node_name + " by " + " " + props[i];
                                        else
                                            var resp = node1.node_name + " " + rel._type + " " + node2.node_name + " for " + " " + props[i];
                                        add(resp);

                                    }
                                    if ((key + 1) == relationships.length) {
                                        console.log("key:" + key + relationships.length);
                                        send();
                                    }
                                });

                            }
                        });
                        callback();

                    }, function(err) {
                        // if any of the file processing produced an error, err would equal that error
                        if (err) {
                            // One of the iterations produced an error.
                            // All processing will now stop.
                            console.log('A file failed to process');
                        } else {
                            console.log('All files have been processed successfully');
                        }
                    });


                });

            }
        });
    } else {
        er = '{"err":"Fields cant be empty"}';
        var e = JSON.parse(er);
        res.json(e);

    }
});

/**
 * Log a user out of their account, then redirect them to the home page.
 */
router.post('/relationship', function(req, res) {

    //console.log(req.body);
    if (req.body.node1 != "" && req.body.node != "" && req.body.how != "") {
        var tmp = "";
        models.Rels.find({ $and: [{ node1: req.body.node1 }, { node2: req.body.node2 }] }, function(err, node) {
            if (err) throw err;
            if (node.length == 1)
                tmp = node[0].how;
            console.log(node.how == req.body.label);
            if ((node.length >= 2) || (tmp == req.body.label)) {
                console.log("relationship already exist");
                res.json('');

            } else {

                models.Node.findOne({ node_name: req.body.node1 }, function(err, node1) {
                    if (err) throw err;
                    //console.log(node1.node_id);
                    if (node1) {

                        models.Node.findOne({ node_name: req.body.node2 }, function(err, node2) {
                            if (err) throw err;
                            //console.log(node2.node_id);
                            if (node2) {

                                var how = req.body.how;
                                var json_how = "{";
                                howparts = how.split(/\s*,\s*/);
                                json_how = '{"prop":' + '"' + how + '"}';

                                db.insertRelationship(node1.node_id, node2.node_id, req.body.label, JSON.parse(json_how), function(err, relationship) {
                                    if (err) throw err;

                                    // Output relationship properties.

                                    var rels = new models.Rels({
                                        node1: req.body.node1,
                                        node2: req.body.node2,
                                        rel_id: relationship._id,
                                        how: req.body.label,

                                    });
                                    rels.save(function(err) {
                                        //console.log('hi');
                                        if (err)
                                            console.log(err);
                                    });

                                    res.json(relationship);
                                });

                            } else {
                                console.log("search failure node2");
                                res.json('');
                            }

                        });
                    } else {
                        console.log("search failure node1");
                        res.json('');
                    }

                });
            }

        });
    } else {
        er = '{"err":"Fields cant be empty"}';
        var e = JSON.parse(er);
        res.json(e);

    }


});

router.post('/update', function(req, res) {

    if (req.body.node1 != "" && req.body.node != "" && req.body.how != "") {

        models.Rels.findOne({ $and: [{ node1: req.body.node1 }, { node2: req.body.node2 }, { how: req.body.label }] }, function(err, node) {
            if (err) throw err;
            //console.log(node.rel_id);

            var how = req.body.how;
            var json_how = "{";
            howparts = how.split(/\s*,\s*/);
            json_how = '{"prop":' + '"' + how + '"}';
            //console.log(JSON.parse(json_how));
            if (node) {
                db.updateRelationship(node.rel_id, JSON.parse(json_how), function(err, relationship) {
                    console.log(relationship);
                    if (err) throw err;
                    if (relationship === true) {
                        res.json(relationship);
                        console.log("relationship updated");
                    } else {
                        console.log("relationship not found, hence not updated.");
                    }
                });

            } else
                res.json('');
        });
    } else {
        er = '{"err":"Fields cant be empty"}';
        var e = JSON.parse(er);
        res.json(e);

    }

});

router.post('/view', function(req, res) {

    models.Rels.findOne({ $and: [{ node1: req.body.node1 }, { node2: req.body.node2 }, { how: req.body.label }] }, function(err, node) {
        if (err) throw err;
        //console.log(req.body.node1+req.body.node2+req.body.label+node);

        if (node) {
            db.readRelationship(node.rel_id, function(err, relationship) {
                if (err) throw err;
                res.json(relationship);
            });
        } else
            res.json('');
    });

});

router.post('/delete', function(req, res) {

    models.Rels.findOneAndRemove({ $and: [{ node1: req.body.node1 }, { node2: req.body.node2 }, { how: req.body.label }] }, function(err, node) {
        if (err) throw err;
        if (node) {
            db.deleteRelationship(node.rel_id, function(err, relationship) {
                if (err) throw err;

                if (relationship === true) {
                    msg = '{"msg":"Relationship ' + req.body.label + ' between ' + req.body.node1 + ' and ' + req.body.node2 + ' deleted succesfully!"}';
                    res.json(JSON.parse(msg));
                } else {
                    res.json('');
                }
            });
        }
    });

});


router.post('/delete_node', function(req, res) {

    var flag = 0;




    db.cypherQuery("match (n)-[r]->(p) where n.name='" + req.body.name + "' delete r", function(err, result) {
        if (err) throw err;
        if (result) {
            console.log('success');
            db.cypherQuery("match (n)-[r]->(p) where p.name='" + req.body.name + "' delete r", function(err, result) {
                if (err) throw err;
                if (result) {
                    console.log('success');
                    models.Rels.find({ $or: [{ node1: req.body.name }, { node2: req.body.name }] }).remove().exec(function(err, data) {
                        // data will equal the number of docs removed, not the document itself
                        console.log(data + "removved");
                        models.Node.findOneAndRemove({ $and: [{ node_name: req.body.name }, { label: req.body.label }] }, function(err, node) {
                            if (err) throw err;
                            if (node) {
                                console.log('node removed');


                                db.deleteNode(node.node_id, function(err, node) {
                                    if (err) throw err;
                                    if (node === true) {
                                        console.log("node removed from neo4j")
                                        res.json(node);

                                    } else {

                                    }
                                });
                            }
                        });
                    });

                }
            });

        }
    });



});

router.post('/response', function(req, res) {
    var k = 0,
        flag = 1,
        rl;
    var arr = [];
    //console.log(req.params.id);
    var relation = [];
    var harms = [];
    var obj = [];
    var para = req.body.input.split(/\s*,\s*/);
    var resp = '{"resp":"';
    //console.log(para);
    var send = function() {
        //console.log(relation.length);
        if (flag) {
            flag = 0;
            for (i = 0; i < relation.length; i++) {
                console.log(relation[i]);
                if (relation[i]._type == 'Harms') {
                    console.log(obj[relation[i]._start] + " " + relation[i]._type + " " + obj[relation[i]._end] + " " + ' by ' + relation[i].prop);
                    resp = resp + obj[relation[i]._start] + " " + relation[i]._type + " " + obj[relation[i]._end] + " " + ' by ' + relation[i].prop;
                    harms.push(relation[i]);
                } else {
                    console.log(obj[relation[i]._start] + " " + relation[i]._type + " " + obj[relation[i]._end] + " " + ' for ' + relation[i].prop);
                    resp = resp + obj[relation[i]._start] + " " + relation[i]._type + " " + obj[relation[i]._end] + " " + ' for ' + relation[i].prop;
                }
                if (i != relation.length - 1)
                    resp += '.<br/>';
            }
            resp += '"}'
            result=JSON.parse(resp);
            res.json(JSON.parse(resp));



        }
    }

    models.Rels.find({ $and: [{ node1: { $in: para } }, { node2: { $in: para } }] }, function(err, node) {
        if (err) throw err;
        if (node)
            rl = node.length;
        if (rl == 0) {
            msg = '{"err":"No relationship exist between them in the database.Update dabase if needed."}';
            res.json(JSON.parse(msg));
        }
        console.log('fuck' + rl)
    });


    models.Node.find({ node_name: { $in: para } }, function(err, node) {
        if (err) throw err;
        if (node) {
            console.log('say' + node.length + node);
            for (i = 0; i < node.length; i++) {
                obj[node[i].node_id] = node[i].node_name;
                console.log(node[i].node_id + ":" + node[i].node_name);
            }
            console.log('obj:' + obj.length);
        }
    });

    if (para.length == 1)
        console.log(para[0]);

    for (i = 0; i < para.length; i++) {
        for (j = 0; j < para.length; j++) {
            if (i != j)
                arr.push({ val1: para[i], val2: para[j] });

        }
    }

    n = para.length;
    console.log('arrrr ' + arr.length);

    async.forEachOf(arr, function(a, i, callback) {
        var p = i;
        console.log('return :' + i);
        console.log(a.val1 + ":" + a.val2);
        models.Rels.find({ $and: [{ node1: a.val1 }, { node2: a.val2 }] }, function(err, node) {
            if (err) throw err;
            //console.log('here'+node);
            if (node) {
                //console.log(arr[p]+'node'+arr[p+1]+" "+node);
                async.forEachOf(node, function(rel, key, callback) {
                    db.readRelationship(rel.rel_id, function(err, relationship) {
                        if (err) throw err;
                        if (relationship) {
                            console.log('inserting' + relationship._start + " " + relationship._end + " " + JSON.stringify(relationship));
                            relation.push(relationship);

                            console.log('objl' + obj.length);

                            if (relation.length == rl) {
                                console.log('para' + para.length);
                                console.log('calling send' + p + "i" + k + "k")
                                send();
                            }

                        }

                    });


                }, function(err) {
                    if (err) {

                        console.log('A file failed to process');
                    } else {
                        console.log('All files have been processed successfully');
                    }
                    // configs is now a map of JSON data
                });
            } else {
                console.log('hwy there hehe');
                //res.json('');
            }
        });
        callback();

    }, function(err) {
        if (err) {
            console.log('A file failed to process');
        } else {
            console.log('All files have been processed successfully');
        }
    });
});




router.get('/logout', function(req, res) {
    if (req.session) {
        req.session.reset();
    }
    res.redirect('/');
});


module.exports = router;
