var express = require('express');
var router = express.Router();
var converts = require('../service/convertRestoGraph');
var authorize = require('../service/authorize');
var getJSON = require('../getJSON');
var stringHelper = require('../helpers/stringHelper');


router.post('/relatednodes', function(req, res) {
 
    nodeid = req.param('nodeid');
    if(nodeid == undefined){
        res.send({error: "No nodeid sent"});
        return;
    }
    var post_data = stringHelper.parseString('{"statements":[{"statement":"start me = node(%s) match path = (me)-[]-(others) return path","resultDataContents":["graph"]}]}', nodeid);
    var options = {
        host: 'localhost',
        port: 7474,
        path: '/db/data/transaction/commit',
        method: 'POST',
        headers: {
            'Content-Type': 'application/json',
            'Content-Length': post_data.length,
            'Authorization': authorize.getNeo4jAuthorizationHeader()
        }
    };
    getJSON.getJSON(options, post_data,
        function(statusCode, result) {
            if (statusCode == 200) {} else {}
            var temp = converts.convertGraph(result);
            res.send(temp);
        });
});

router.post('/getqueryresults', function(req, res) {
    var query = req.param('query');
    var post_data = stringHelper.parseString('{"statements":[{"statement":"%s","resultDataContents":["graph"]}]}', query);
    var options = {
        host: 'localhost',
        port: 7474,
        path: '/db/data/transaction/commit',
        method: 'POST',
        headers: {
            'Content-Type': 'application/json',
            'Content-Length': post_data.length,
            'Authorization': authorize.getNeo4jAuthorizationHeader()
        }
    };
    getJSON.getJSON(options, post_data,
        function(statusCode, result) {
            if (statusCode == 200) {} else {}
            var temp = converts.convert(result);
            res.send(temp);
        });
});

router.post('/', function(req, res) {
    
    var post_data = '{"statements":[{"statement":"MATCH path = (n)-[r]->(m) RETURN path limit 10 ","resultDataContents":["graph"]}]}';
    var query = "";
    var contents = "";

            query = 'match (c) return c limit 10';
            contents = '"graph"';

    post_data = stringHelper.parseString(post_data, query,contents);
    var getJSON = require('../getJSON');
    var options = {
        host: 'localhost',
        port: 7474,
        path: '/db/data/transaction/commit',
        method: 'POST',
        headers: {
            'Content-Type': 'application/json',
            'Content-Length': post_data.length,
            'Authorization': authorize.getNeo4jAuthorizationHeader()
        }
    };
    getJSON.getJSON(options, post_data,
        function(statusCode, result) {
            if (statusCode == 200) {} else {}
            var temp;
            if(contents == '"graph"'){
               temp = converts.convertGraph(result);
            }else{
                temp = converts.convertTable(result);
            }
            res.send(temp);
        });
});

function parseString(str) {
    var args = [].slice.call(arguments, 1),
        i = 0;

    return str.replace(/%s/g, function() {
        return args[i++];
    });
}

module.exports = router;
