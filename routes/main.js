var express = require('express');

var utils = require('../utils');

var router = express();

/**
 * Render the home page.
 */
router.get('/', function(req, res) {
  res.render('index');
});



/**
 * Render the dashboard page.
 */
router.get('/contribute', utils.requireLogin, function(req, res) {
  res.render('contribute');
});



module.exports = router;
