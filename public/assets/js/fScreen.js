document.addEventListener("keydown", function(e) {
  if (e.keyCode == 13) {
    toggleFullScreen();
    document.getElementById("graphview").setAttribute("class","fullClass");
     d3.select("#graphview")
                .call(graphView).attr("width", window.innerWidth).attr("height", window.innerHeight);
  }
}, false);



function toggleFullScreen() {
			var elem = document.getElementById("graphview");
		if (elem.requestFullscreen) {
		  elem.requestFullscreen();
		} else if (elem.msRequestFullscreen) {
		  elem.msRequestFullscreen();
		} else if (elem.mozRequestFullScreen) {
		  elem.mozRequestFullScreen();
		} else if (elem.webkitRequestFullscreen) {
		  elem.webkitRequestFullscreen();
		}
}