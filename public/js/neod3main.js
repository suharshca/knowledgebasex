d3.xhr("../data/style.grass", "application/grass", function(request) {
    drawGraph(request.responseText);
});
//when changing this do change in neod3 file also
var widthGraphView = 900,
    heightGraphView = 600;



var drawGraph = function(styleContents) {
    var graphView = neo.graphView()
        .style(styleContents)
        .on('nodeClicked', function(node) {
            var type = node.labels;
            nodes = node.toJSON();
            if (!Object.keys) Object.keys = function(o) {
                if (o !== Object(o))
                    throw new TypeError('Object.keys called on non-object');
                var ret = [],
                    p;
                for (p in o)
                    if (Object.prototype.hasOwnProperty.call(o, p)) ret.push(p);
                return ret;
            }

            /*var keys = Object.keys(nodes);

            var tableRef = document.getElementById('nodepropertytable').getElementsByTagName('tbody')[0];
            tableRef.innerHTML = '';
            var newRow = tableRef.insertRow(tableRef.rows.length);
            var newHeader = document.createElement("th");
            newHeader.innerHTML = 'Type';
            newRow.appendChild(newHeader);

            newRow = tableRef.insertRow(tableRef.rows.length);
            var newCell = newRow.insertCell(0);
            var newText = document.createTextNode(type);
            newCell.appendChild(newText);

            for (var i = keys.length - 1; i >= 0; i--) {
                var newRow = tableRef.insertRow(tableRef.rows.length);
                var newHeader = document.createElement("th");
                newHeader.innerHTML = keys[i];
                newRow.appendChild(newHeader);

                newRow = tableRef.insertRow(tableRef.rows.length);
                var newCell = newRow.insertCell(0);
                var newText = document.createTextNode(nodes[keys[i]]);
                newCell.appendChild(newText);
            }*/
        })
        .on('nodeDblClicked', function(node) {
            console.log("Testing : " + JSON.stringify(node));
            $.post("/api/graph/relatednodes", {
                nodeid: node.id
            })

            .done(function(data) {
                if ('error' in data) {
                    alert(data.error);
                    return;
                }
                graphModel.nodes.add(data.nodes);
                graphModel.relationships.add(data.links);
            });
        })
        .on('relationshipClicked', function(relationship) {
            var relation = relationship.toJSON();
            alert(relation.prop);
        });

    var graphModel;
    var apiUri = "/api/graph/";
    $.post(apiUri, function(data) {
        if ("nodes" in data) {
            graphModel = neo.graphModel().nodes(data.nodes).relationships(data.links);
            d3.select("#graphview")
                .data([graphModel])
                .call(graphView).attr("width", widthGraphView).attr("height", heightGraphView);

        } else {
            var numColumns = data.columns.length;
            var tableRef = document.getElementById('main_table').getElementsByTagName('tbody')[0];
            tableRef.innerHTML = '';
            var newRow = tableRef.insertRow(tableRef.rows.length);
            for (var i = 0; i < numColumns; i++) {
                var newHeader = document.createElement("th");
                newHeader.innerHTML = data.columns[i];
                newRow.appendChild(newHeader);
            }
            var numRows = data.rows.length;
            for (var i = 0; i < numRows; i++) {

                newRow = tableRef.insertRow(tableRef.rows.length);
                for (var j = 0; j < numColumns; j++) {
                    var newCell = newRow.insertCell(j);
                    var newText = document.createTextNode(data.rows[i][j]);
                    newCell.appendChild(newText);
                }

            }
            $('#tab-graph-switch').click();
            document.getElementById("tab-graph-switch").disabled = true;
        }
    });

}
