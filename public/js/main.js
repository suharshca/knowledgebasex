var $address="http://localhost:3000/";

//"http://ec2-52-38-8-254.us-west-2.compute.amazonaws.com:3000/"

$('#create_node').click(function(event){
  var $name=$('#create_nod1');
  var $sel1=$('#create_sel');
  var $msg=$('#create_msg');
  node={
       name:$name.val(),
       label:$sel1.val(),
     };
$.ajax({
    url : $address+"insert",
    type: "POST",
    dataType: "json",
    data:node,

     success : function(data) {
        $msg.empty();
        if(data.err)
         {
           $msg.append(data.err);
         }
         else if(data)
         {
            $msg.append('Node '+$name.val()+' added to the Database.Thank you!');
          }
          else
          {
            $msg.append('Node already exist');
          }

     },
    error: function (jqXHR, textStatus, errorThrown) {
      console.log(jqXHR, textStatus, errorThrown);
      $msg.empty();
      $msg.append('failure');
    }
    
});
});

$('#delete_node').click(function(event){
  var $name=$('#create_nod1');
  var $sel1=$('#create_sel');
  var $msg=$('#create_msg');
  node={
       name:$name.val(),
       label:$sel1.val(),
     };
$.ajax({
    url : $address+"delete_node",
    type: "POST",
    dataType: "json",
    data:node,

     success : function(data) {
        $msg.empty();
        if(data.err)
         {
           $msg.append(data.err);
         }
         else if(data)
         {
            $msg.append('Node '+$name.val()+' deleted from the Database.Thank you!');
          }
          else
          {
            $msg.append('Node doesnt exist');
          }

     },
    error: function (jqXHR, textStatus, errorThrown) {
      console.log(jqXHR, textStatus, errorThrown);
      $msg.empty();
      $msg.append('failure');
    }
    
});
});

$('#create_realtionship').click(function(event){
  var $node1=$('#rel_nod1');
  var $node2=$('#rel_nod2')
  var $sel1=$('#rel_sel');
  var $how=$('#rel_how');
  var $msg=$('#rel_msg');
  node={
       node1:$node1.val(),
       node2:$node2.val(),
       how:$how.val(),
       label:$sel1.val(),
     };
$.ajax({
    url : $address+"relationship",
    type: "POST",
    dataType: "json",
    data:node,

     success : function(data) {
        $msg.empty();
        if(data.err)
         {
           $msg.append(data.err);
         }
         else if(data)
         {
            $msg.append('Relationship '+$sel1.val()+' between '+$node1.val()+' and '+$node2.val()+ ' is added to the database.Thank you!');
          }
          else
          {
            $msg.append('Relationship might be already existing.Use "See Relationship" with  "how" field empty to see what relationship exist.Press "Update relationship" to update');
          }

     },
    error: function (jqXHR, textStatus, errorThrown) {
      console.log(jqXHR, textStatus, errorThrown);
      $msg.empty();
      $msg.append('failure');
    }
    
});
});

$('#search_node').click(function(event){
  var $name=$('#search_nod1');
  var $msg=$('#serach_msg');
  node={
       name:$name.val(),
     };
$.ajax({
    url : $address+"search",
    type: "POST",
    dataType: "json",
    data:node,

     success : function(data) {
        $msg.empty();
        if(data.err)
         {
           $msg.append(data.err);
         }
         else if(data.msg)
         {
           $msg.append(data.msg);
         }
          else if(data)
          {
            $msg.append('Search Results : '+data.resp);
          }
          else
          {
            $msg.append('No node found');
          }

     },
    error: function (jqXHR, textStatus, errorThrown) {
      console.log(jqXHR, textStatus, errorThrown);
      $msg.empty();
      $msg.append('failure');
    }
    
  });
});


$('#search').click(function(event){
  var $name=$('#search_input');
  var $msg=$('#msg');
  node={
       name:$name.val(),
     };
$.ajax({
    url : $address+"search",
    type: "POST",
    dataType: "json",
    data:node,

     success : function(data) {
        $msg.empty();
        if(data.err)
         {
           $msg.append(data.err);
         }
          else if(data.msg)
          {
           $msg.append(data.msg);
          }
          else if(data)
          {
            $msg.append('Search Results : '+data.resp);
          }
          else
          {
            $msg.append('No node found');
          }

     },
    error: function (jqXHR, textStatus, errorThrown) {
      console.log(jqXHR, textStatus, errorThrown);
      $msg.empty();
      $msg.append('failure');
    }
    
  });
});


$('#update_realtionship').click(function(event){
  var $node1=$('#rel_nod1');
  var $node2=$('#rel_nod2')
  var $sel1=$('#rel_sel');
  var $how=$('#rel_how');
  var $msg=$('#rel_msg');
  node={
       node1:$node1.val(),
       node2:$node2.val(),
       how:$how.val(),
       label:$sel1.val(),
     };
$.ajax({
    url : $address+"update",
    type: "POST",
    dataType: "json",
    data:node,

     success : function(data) {
        $msg.empty();
         if(data.err)
         {
           $msg.append(data.err);
         }
         else if(data)
         {
            $msg.append('Relationship '+$sel1.val()+' is updated in the database.Thank you!'+data);
          }
          else
          {
            $msg.append('Error encountered.Relationship might be already existing(To check make use of the search option),try again later otherwise.');
          }

     },
    error: function (jqXHR, textStatus, errorThrown) {
      console.log(jqXHR, textStatus, errorThrown);
      $msg.empty();
      $msg.append('Failure!Possibly due to one or more fields left empty.');
    }
  });
});

$('#see_realtionship').click(function(event){
  var $node1=$('#rel_nod1');
  var $node2=$('#rel_nod2')
  var $sel1=$('#rel_sel');
  //var $how=$('#rel_how');
  var $msg=$('#rel_msg');
  node={
       node1:$node1.val(),
       node2:$node2.val(),
       //how:$how.val(),
       label:$sel1.val(),
     };
$.ajax({
    url : $address+"view",
    type: "POST",
    dataType: "json",
    data:node,

     success : function(data) {
        $msg.empty();
          if(data)
          {
            document.getElementById("rel_how").value = data.prop;
            $msg.append('Relationship set in "how" field!');
          }
          else
          {
            $msg.append('No relationship exist');
          }

     },
    error: function (jqXHR, textStatus, errorThrown) {
      console.log(jqXHR, textStatus, errorThrown);
      $msg.empty();
      $msg.append('Failure!Possibly due to one or more fields left empty.');
    }
  });
});

$('#delete_realtionship').click(function(event){
  var $node1=$('#rel_nod1');
  var $node2=$('#rel_nod2')
  var $sel1=$('#rel_sel');
  //var $how=$('#rel_how');
  var $msg=$('#rel_msg');
  node={
       node1:$node1.val(),
       node2:$node2.val(),
       //how:$how.val(),
       label:$sel1.val(),
     };
$.ajax({
    url : $address+"delete",
    type: "POST",
    dataType: "json",
    data:node,

     success : function(data) {
        $msg.empty();
          if(data)
          {
            $msg.append(data.msg);
          }
          else
          {
            $msg.append('Error encountered.Relationship might be already existing(To check make use of the search option),try again later otherwise.');
          }

     },
    error: function (jqXHR, textStatus, errorThrown) {
      console.log(jqXHR, textStatus, errorThrown);
      $msg.empty();
      $msg.append('Failure!Possibly due to one or more fields left empty.');
    }
  });
});


$('#response').click(function(event){
  var $input=$('#node_in');
  var $msg=$('#response_msg');
  node={
        input:$input.val(),
     };
$.ajax({
    url : $address+"response",
    type: "POST",
    dataType: "json",
    data:node,

     success : function(data) {
        $msg.empty();
        if(data.err)
         {
           $msg.append(data.err);
         }
         else if(data.empty)
         {
          $msg.append(data.empty)
         }
         else if(data)
         {
            $msg.append('Response Recieved : <br/>'+data.resp);
          }
          else
          {
            $msg.append('Node already exist');
          }

     },
    error: function (jqXHR, textStatus, errorThrown) {
      console.log(jqXHR, textStatus, errorThrown);
      $msg.empty();
      $msg.append('failure');
    }
    
});
});