var mongoose = require('mongoose');

var Schema = mongoose.Schema;
var ObjectId = Schema.ObjectId;


module.exports.User = mongoose.model('User', new Schema({
  id:           ObjectId,
  firstName:    { type: String, required: '{PATH} is required.' },
  lastName:     { type: String, required: '{PATH} is required.' },
  email:        { type: String, required: '{PATH} is required.', unique: true },
  password:     { type: String, required: '{PATH} is required.' },
  data:         Object,
}));

module.exports.Node = mongoose.model('Node', new Schema({
  id:           ObjectId,
  node_id:    { type: String, required: '{PATH} is required.' },
  node_name:     { type: String, required: '{PATH} is required.' },
  label :{ type: String, required: '{PATH} is required.' },
  data:         Object,
}));

module.exports.Rels=mongoose.model('Rels',new Schema({
	id:  ObjectId,
	node1:{ type: String, required: '{PATH} is required.' },
	node2:{ type: String, required: '{PATH} is required.' },
	rel_id:{ type: String, required: '{PATH} is required.' },
	how:{ type: String, required: '{PATH} is required.' },
	data: Object,
}));




